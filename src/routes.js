import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import Details from './pages/Details';
import Home from './pages/Home';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/details/:id" component={Details} />
        <Route exact path="/home" component={Home} />
        <Redirect to="/home" />
      </Switch>
    </Router>
  );
};

export default Routes;
