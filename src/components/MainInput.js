import React, { useState } from 'react';

const MainInput = () => {
  const [name, setName] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    setName(e.target.value);
  };

  return (
    <div>
      <input placeholder="Ingrese su nombre..." value={name} o />
    </div>
  );
};

export default MainInput;
