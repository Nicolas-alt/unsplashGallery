import React from 'react';
import '../assets/styles/NavBar.css';
import image from '../assets/logo.svg';

const NavBar = () => {
  return (
    <nav className="nav">
      <img className="nav-img" src={image} alt="Nav icon" />
      <form className="nav-form">
        <div className="nav-form-div">
          <i className="bx bx-search-alt-2"></i>
          <input
            className="nav-form-div-input"
            placeholder="Search something..."
          />
        </div>
      </form>
      <div className="nav-div-user">
        <span>N</span>
      </div>
    </nav>
  );
};

export default NavBar;
