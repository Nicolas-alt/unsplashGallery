import React, { useEffect, useState } from 'react';
import CustomHelmet from '../components/CustomHelmet';
import NavBar from '../components/NavBar';
import { getPhotos } from '../helpers/getPhotos';
import '../assets/styles/Home.css';

const Home = () => {
  const [photos, setPhotos] = useState([]);
  useEffect(() => {
    getPhotos().then((data) => setPhotos(data));
  }, []);

  console.log(photos);

  return (
    <>
      <CustomHelmet title="Home" />
      <NavBar />
      <section className="home-section">
        <h1>Home</h1>
      </section>
    </>
  );
};

export default Home;
