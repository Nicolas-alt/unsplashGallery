import { keys } from '../keys/keys';
const url = `${process.env.REACT_APP_API_URL}=${keys.access}`;

export const getPhotos = async () => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return data.map((image) => {
      return {
        imageData: {
          alt: image.alt_description,
          url: image.urls.regular,
          description: image.description,
          totalLikes: image.likes,
        },
        userData: {
          userBio: image.user.bio,
          name: image.user.username,
          image: image.user.profile_image.small,
          totalPhotos: image.user.total_photos,
          twitter: image.user.twitter_username,
          instagram: image.user.instagram_username,
          portafolio: image.user.portfolio_url,
        },
      };
    });
  } catch (error) {
    console.error(error);
  }
};
